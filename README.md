# Summary

This is Android SDK For WeQ Studios. It provides API to send data to WeQ Studios Analytics services. The API only provides low level device specific methods. SDK integrates with WeQ Unity plugin.