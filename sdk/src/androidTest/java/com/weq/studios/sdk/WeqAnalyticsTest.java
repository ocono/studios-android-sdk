package com.weq.studios.sdk;

import androidx.test.core.app.ActivityScenario.ActivityAction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import com.google.gson.Gson;
import com.weq.studios.sdk.common.HttpClient;
import com.weq.studios.sdk.common.HttpClient.HttpResponse;
import com.weq.studios.sdk.trackingevent.model.CustomTag;
import com.weq.studios.sdk.trackingevent.model.EventDefinition;
import com.weq.studios.sdk.common.model.WeqConfig;
import com.weq.studios.sdk.trackingevent.model.WeqEvent;
import com.weq.studios.sdk.trackingevent.model.TrackingEventBatch;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

@RunWith(AndroidJUnit4.class)
public class WeqAnalyticsTest {

    @Rule
    public ActivityScenarioRule<TestActivity> rule = new ActivityScenarioRule<>(TestActivity.class);

    private Gson gson = new Gson();

    @Test
    public void testThatItPublishesEvent() {
        rule.getScenario().onActivity(new ActivityAction<TestActivity>() {
            @Override
            public void perform(TestActivity activity) {
                HttpClient httpClientMock = Mockito.mock(HttpClient.class);
                ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

                Mockito.when(httpClientMock.post(Mockito.anyString(), Mockito.anyString()))
                    .thenReturn(new HttpResponse("", 202));

                WeqAnalytics.setHttpClient(httpClientMock);
                WeqAnalytics.configure(new WeqConfig(activity, "app-token", "game-id"));

                List<CustomTag> tags = new ArrayList<>();
                tags.add(new CustomTag("tag.1", "1"));
                tags.add(new CustomTag("tag.2", "2"));

                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(1));

                    WeqAnalytics.event(new WeqEvent(new EventDefinition("category", "name", tags)));
                    WeqAnalytics.event(new WeqEvent(new EventDefinition("category2", "name2", tags)));

                    Thread.sleep(TimeUnit.SECONDS.toMillis(5));

                    Mockito.verify(httpClientMock, Mockito.atLeastOnce()).post(captor.capture(), captor.capture());
                    TrackingEventBatch trackingEvent = gson.fromJson(captor.getValue(), TrackingEventBatch.class);
                    Assert.assertEquals(2, trackingEvent.getEvents().size());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
