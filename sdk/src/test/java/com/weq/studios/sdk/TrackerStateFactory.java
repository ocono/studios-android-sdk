package com.weq.studios.sdk;

import android.app.Activity;
import android.content.Context;

import com.weq.studios.sdk.common.model.SdkConfiguration;
import com.weq.studios.sdk.common.model.Session;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.common.model.WeqConfig;

import org.mockito.Mockito;

import java.util.Properties;

public class TrackerStateFactory {

    public static TrackerState create() {
        Activity activityMock = Mockito.mock(Activity.class);
        Context context = Mockito.mock(Context.class);
        Mockito.when(activityMock.getApplicationContext()).thenReturn(context);

        WeqConfig weQConfig = createDefaultWeqConfiguration(activityMock);
        SdkConfiguration sdkConfiguration = createDefaultSdkConfiguration();
        Session session = createDefaultSession();
        session.setUserId("test.user.1");

        return new TrackerState(weQConfig, sdkConfiguration, session);
    }

    private static WeqConfig createDefaultWeqConfiguration(Activity activityMock) {
        return new WeqConfig(activityMock, "test-app-token", "test-game-id");
    }

    private static Session createDefaultSession() {
        return new Session();
    }

    private static SdkConfiguration createDefaultSdkConfiguration() {
        Properties properties = new Properties();
        properties.setProperty(SdkConfiguration.PROPERTY_WEQ_SDK_EVENT_FLUSH_BATCH_SIZE, "5");
        properties.setProperty(SdkConfiguration.PROPERTY_WEQ_SDK_EVENT_FLUSH_FREQUENCY_IN_SECONDS, "1");
        properties.setProperty(SdkConfiguration.PROPERTY_WEQ_SDK_EVENT_INGRESS_HOSTNAME, "http://localhost");
        properties.setProperty(SdkConfiguration.PROPERTY_WEQ_SDK_EVENT_STORAGE_DELETE_OLDEST, "2");
        properties.setProperty(SdkConfiguration.PROPERTY_WEQ_SDK_EVENT_STORAGE_SIZE_LIMIT, "5");
        properties.setProperty(SdkConfiguration.PROPERTY_WEQ_SDK_PARAMETER_STORE_UPDATE_FREQUENCY_IN_SECONDS, "5");

        return new SdkConfiguration(properties);
    }

}
