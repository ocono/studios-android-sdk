package com.weq.studios.sdk;

import com.weq.studios.sdk.common.HttpClient;
import com.weq.studios.sdk.common.HttpClient.HttpResponse;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.trackingevent.EventPublisher;
import com.weq.studios.sdk.trackingevent.EventRepository;
import com.weq.studios.sdk.trackingevent.model.EventDefinition;
import com.weq.studios.sdk.trackingevent.model.EventValue;
import com.weq.studios.sdk.trackingevent.model.TrackingAppInfo;
import com.weq.studios.sdk.trackingevent.model.TrackingConnection;
import com.weq.studios.sdk.trackingevent.model.TrackingDeviceInfo;
import com.weq.studios.sdk.trackingevent.model.TrackingEvent;

import com.weq.studios.sdk.trackingevent.model.TrackingSession;
import com.weq.studios.sdk.trackingevent.model.TrackingTime;
import com.weq.studios.sdk.trackingevent.model.WeqEvent;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class EventPublisherTest {

    @Test
    public void testThatItSendsEvents() throws InterruptedException {
        EventRepository eventRepositoryMock = Mockito.mock(EventRepository.class);
        HttpClient httpClientMock = Mockito.mock(HttpClient.class);
        TrackerState trackerState = TrackerStateFactory.create();

        Map<String, TrackingEvent> events = new HashMap<>();
        events.put("event.1", createTrackingEvent());

        Mockito.when(eventRepositoryMock.getEvents(Mockito.anyInt()))
            .thenReturn(events);

        Mockito.when(httpClientMock.post(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(new HttpResponse("", 202));

        EventPublisher eventPublisherSpy = Mockito.spy(new EventPublisher(eventRepositoryMock, trackerState, httpClientMock));
        Mockito.doReturn(true).when(eventPublisherSpy).isConnected();
        eventPublisherSpy.start();

        Thread.sleep(TimeUnit.SECONDS.toMillis(20));

        Mockito.verify(httpClientMock, Mockito.atLeastOnce()).post(Mockito.anyString(), Mockito.anyString());
    }

    private TrackingEvent createTrackingEvent() {
        return new TrackingEvent(
            "",
            new TrackingSession(),
            new WeqEvent(new EventDefinition(), new EventValue()),
            new TrackingTime(""),
            "",
            new TrackingAppInfo(),
            new TrackingDeviceInfo(),
            new TrackingConnection()
        );
    }

    @Test
    public void testThatItClearsOldEvents() throws InterruptedException {
        EventRepository eventRepositoryMock = Mockito.mock(EventRepository.class);
        HttpClient httpClientMock = Mockito.mock(HttpClient.class);
        TrackerState trackerState = TrackerStateFactory.create();

        Map<String, TrackingEvent> events = new HashMap<>();
        events.put("event.1", createTrackingEvent());

        Mockito.when(eventRepositoryMock.getEvents(Mockito.anyInt()))
            .thenReturn(events);

        Mockito.when(httpClientMock.post(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(new HttpResponse("", 202));

        EventPublisher eventPublisherSpy = Mockito.spy(new EventPublisher(eventRepositoryMock, trackerState, httpClientMock));
        Mockito.doReturn(true).when(eventPublisherSpy).isConnected();
        eventPublisherSpy.start();

        Thread.sleep(TimeUnit.SECONDS.toMillis(20));

        Mockito.verify(eventRepositoryMock, Mockito.atLeastOnce()).clearByIds(Mockito.anySetOf(String.class));
    }

    @Test
    public void testThatItStopsTrackerWhenTrackingIsDisabled() throws InterruptedException {
        EventRepository eventRepositoryMock = Mockito.mock(EventRepository.class);
        HttpClient httpClientMock = Mockito.mock(HttpClient.class);
        TrackerState trackerState = TrackerStateFactory.create();

        trackerState.setTrackingEnabled(false);

        Map<String, TrackingEvent> events = new HashMap<>();
        events.put("event.1", new TrackingEvent());

        Mockito.when(eventRepositoryMock.getEvents(Mockito.anyInt()))
            .thenReturn(events);

        EventPublisher eventPublisherSpy = Mockito.spy(new EventPublisher(eventRepositoryMock, trackerState, httpClientMock));
        eventPublisherSpy.start();

        Thread.sleep(TimeUnit.SECONDS.toMillis(20));

        Mockito.verify(eventRepositoryMock, Mockito.never()).clearByIds(Mockito.anySetOf(String.class));
    }

    @Test
    public void testItSendsNothingWhenNoEventsStored() throws InterruptedException {
        EventRepository eventRepositoryMock = Mockito.mock(EventRepository.class);
        HttpClient httpClientMock = Mockito.mock(HttpClient.class);
        TrackerState trackerState = TrackerStateFactory.create();

        Map<String, TrackingEvent> events = new HashMap<>();
        Mockito.when(eventRepositoryMock.getEvents(Mockito.anyInt()))
            .thenReturn(events);

        EventPublisher eventPublisherSpy = Mockito.spy(new EventPublisher(eventRepositoryMock, trackerState, httpClientMock));
        eventPublisherSpy.start();

        Thread.sleep(TimeUnit.SECONDS.toMillis(2));

        Mockito.verify(eventRepositoryMock, Mockito.never()).clearByIds(Mockito.anySetOf(String.class));
    }

    @Test
    public void testThatItStopsSendingWhenNetworksIsUnavailable() throws InterruptedException {
        EventRepository eventRepositoryMock = Mockito.mock(EventRepository.class);
        HttpClient httpClientMock = Mockito.mock(HttpClient.class);
        TrackerState trackerState = TrackerStateFactory.create();

        Map<String, TrackingEvent> events = new HashMap<>();
        events.put("event.1", new TrackingEvent());

        Mockito.when(eventRepositoryMock.getEvents(Mockito.anyInt()))
            .thenReturn(events);

        Mockito.when(httpClientMock.post(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(new HttpResponse("", 202));

        EventPublisher eventPublisherSpy = Mockito.spy(new EventPublisher(eventRepositoryMock, trackerState, httpClientMock));
        Mockito.doReturn(false).when(eventPublisherSpy).isConnected();
        eventPublisherSpy.start();

        Thread.sleep(TimeUnit.SECONDS.toMillis(2));

        Mockito.verify(eventRepositoryMock, Mockito.never()).clearByIds(Mockito.anySetOf(String.class));
    }
}
