package com.weq.studios.sdk;

import com.weq.studios.sdk.trackingevent.EventRepository;
import com.weq.studios.sdk.trackingevent.EventsTable;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.trackingevent.model.TrackingEvent;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.TimeUnit;

public class EventRepositoryTest {

    @Test
    public void testThatItSavesEvent() throws InterruptedException {
        TrackerState trackerState = TrackerStateFactory.create();

        EventsTable eventsTableMock = Mockito.mock(EventsTable.class);
        EventRepository repository = new EventRepository(eventsTableMock, trackerState);
        repository.addEvent(new TrackingEvent());

        Thread.sleep(TimeUnit.SECONDS.toMillis(1));

        Mockito.verify(eventsTableMock, Mockito.atLeastOnce())
            .insert(Mockito.any(TrackingEvent.class));

    }

    @Test
    public void testThatItTriggerClearOldIdsWhenLimitReached() throws InterruptedException {
        TrackerState trackerState = TrackerStateFactory.create();
        EventsTable eventsTableMock = Mockito.mock(EventsTable.class);

        Mockito.when(eventsTableMock.count()).thenReturn(1L);

        EventRepository repository = new EventRepository(eventsTableMock, trackerState);

        int i = 0;
        while (i++ < 10) {
            repository.addEvent(new TrackingEvent());
        }

        Thread.sleep(TimeUnit.SECONDS.toMillis(1));

        Mockito.verify(eventsTableMock, Mockito.atLeastOnce())
            .clearExpired(Mockito.anyInt());
    }

    @Test
    public void testThatItDoesNotTrackEventWhenTrackingIsDisabled() throws InterruptedException {
        TrackerState trackerState = TrackerStateFactory.create();
        trackerState.setTrackingEnabled(false);

        EventsTable eventsTableMock = Mockito.mock(EventsTable.class);

        Mockito.when(eventsTableMock.count()).thenReturn(1L);

        EventRepository repository = new EventRepository(eventsTableMock, trackerState);
        repository.addEvent(new TrackingEvent());

        Thread.sleep(TimeUnit.SECONDS.toMillis(1));

        Mockito.verify(eventsTableMock, Mockito.never())
            .insert(Mockito.any(TrackingEvent.class));
    }
}
