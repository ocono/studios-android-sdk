package com.weq.studios.sdk.trackingevent.model;

import java.util.List;

public class EventDefinition {

    private String category;
    private String name;
    private List<CustomTag> tags;

    public EventDefinition(String category, String name, List<CustomTag> tags) {
        this.category = category;
        this.name = name;
        this.tags = tags;
    }

    public EventDefinition() {
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public List<CustomTag> getTags() {
        return tags;
    }
}
