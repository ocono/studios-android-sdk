package com.weq.studios.sdk.trackingevent;

import android.app.Activity;
import android.content.res.Configuration;

import android.util.Log;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.weq.studios.sdk.common.DateTime;
import com.weq.studios.sdk.common.Permission;
import com.weq.studios.sdk.trackingevent.model.ConnectionType;
import com.weq.studios.sdk.trackingevent.model.Size;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.trackingevent.model.TrackingAndroidInfo;
import com.weq.studios.sdk.trackingevent.model.TrackingAppInfo;
import com.weq.studios.sdk.trackingevent.model.TrackingConnection;
import com.weq.studios.sdk.trackingevent.model.TrackingDeviceInfo;
import com.weq.studios.sdk.trackingevent.model.TrackingEvent;
import com.weq.studios.sdk.trackingevent.model.TrackingSession;
import com.weq.studios.sdk.trackingevent.model.WeqEvent;

import com.weq.studios.sdk.trackingevent.model.TrackingTime;
import java.util.Date;
import java.util.UUID;

import github.nisrulz.easydeviceinfo.base.DeviceType;
import github.nisrulz.easydeviceinfo.base.EasyAppMod;
import github.nisrulz.easydeviceinfo.base.EasyBatteryMod;
import github.nisrulz.easydeviceinfo.base.EasyCpuMod;
import github.nisrulz.easydeviceinfo.base.EasyDeviceMod;
import github.nisrulz.easydeviceinfo.base.EasyDisplayMod;
import github.nisrulz.easydeviceinfo.base.EasyIdMod;
import github.nisrulz.easydeviceinfo.base.EasyNetworkMod;

import static android.Manifest.*;

public class EventCollector {

    private static final String LOG_TAG = "Weq_collector";

    private final EasyDeviceMod easyDeviceMod;
    private final EasyBatteryMod batteryMod;
    private final EasyIdMod easyIdMod;
    private final EasyNetworkMod easyNetworkMod;
    private final TrackerState trackerState;
    private final EasyCpuMod easyCpuMod;
    private final EasyDisplayMod easyDisplayMod;
    private final EasyAppMod easyAppMod;
    private final Activity activity;
    private static Info addInfo;
    private TrackingAppInfo trackingAppInfo;
    private TrackingAndroidInfo trackingAndroidInfo;

    public EventCollector(TrackerState trackerState) {
        this.activity = trackerState.getWeQConfig().getActivity();
        this.easyDeviceMod = new EasyDeviceMod(activity);
        this.batteryMod = new EasyBatteryMod(activity);
        this.easyIdMod = new EasyIdMod(activity);
        this.easyNetworkMod = new EasyNetworkMod(activity);
        this.trackerState = trackerState;
        this.easyCpuMod = new EasyCpuMod();
        this.easyDisplayMod = new EasyDisplayMod(activity);
        this.easyAppMod = new EasyAppMod(activity);
    }

    public TrackingEvent createFrom(WeqEvent event) {
        return new TrackingEvent(
            UUID.randomUUID().toString(),
            createTrackingSession(),
            event,
            new TrackingTime(DateTime.now()),
            trackerState.getWeQConfig().getGameId(),
            createTrackingAppInfo(),
            createTrackingDeviceInfo(),
            createTrackingConnection()
        );
    }

    public TrackingDeviceInfo createTrackingDeviceInfo() {
        String[] screenSize = easyDisplayMod.getResolution().split("x");
        return new TrackingDeviceInfo(
            getAddInfo().getId(),
            getIMEI(),
            easyDeviceMod.getOSVersion(),
            easyDeviceMod.getBuildBrand(),
            easyDeviceMod.getModel(),
            new Size(Integer.valueOf(screenSize[0]), Integer.valueOf(screenSize[1])),
            batteryMod.getBatteryPercentage() / 100d,
            getAddInfo().isLimitAdTrackingEnabled(),
            easyDeviceMod.isDeviceRooted(),
            createTrackingAndroidInfo()
        );
    }

    private TrackingAppInfo createTrackingAppInfo() {
        if (this.trackingAppInfo == null) {
            this.trackingAppInfo = new TrackingAppInfo(
                easyAppMod.getPackageName(),
                easyAppMod.getAppVersionCode(),
                easyAppMod.getAppVersion(),
                null
            );
        }

        return trackingAppInfo;
    }

    private TrackingAndroidInfo createTrackingAndroidInfo() {
        if (this.trackingAndroidInfo == null) {
            this.trackingAndroidInfo = new TrackingAndroidInfo(
                getGSFID(),
                getAddInfo().getId(),
                easyDeviceMod.getBoard(),
                easyDeviceMod.getBootloader(),
                easyDeviceMod.getBuildBrand(),
                easyCpuMod.getStringSupportedABIS(),
                getIMEI(),
                easyDeviceMod.getScreenDisplayID(),
                easyDeviceMod.getManufacturer(),
                easyDeviceMod.getModel(),
                easyDeviceMod.getProduct(),
                easyDeviceMod.getBuildTags(),
                mapDevice(easyDeviceMod.getDeviceType(this.activity)),
                mapOrientation(easyDeviceMod.getOrientation(this.activity))
            );
        }

        return this.trackingAndroidInfo;
    }

    private Info getAddInfo() {
        if (addInfo != null) {
            return addInfo;
        }
        Info info;
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(activity.getApplicationContext());
        } catch (Exception e) {
            Log.e(LOG_TAG, "An error occurred while reading GAID", e);
            return new Info(null, true);
        }

        addInfo = info;
        return addInfo;
    }


    private TrackingConnection createTrackingConnection() {
        if (Permission.isGranted(activity.getApplicationContext(), permission.ACCESS_NETWORK_STATE) &&
            Permission.isGranted(activity.getApplicationContext(), permission.INTERNET)
        ) {
            return new TrackingConnection(easyNetworkMod.getIPv4Address(), getConnectionType());
        }
        return null;
    }

    private TrackingSession createTrackingSession() {
        return new TrackingSession(
            trackerState.getSession().getSessionId(),
            getAddInfo().getId(),
            trackerState.getSession().getUserId(),
            DateTime.format(new Date(trackerState.getSession().getStart())),
            DateTime.now(),
            getIMEI(),
            null
        );
    }

    private ConnectionType getConnectionType() {
        return ConnectionType.getConnectionTypeById(easyNetworkMod.getNetworkType());
    }

    private String getGSFID() {
        if (Permission.isGranted(activity.getApplicationContext(), "com.google.android.providers.gsf.permission.READ_GSERVICES")) {
            return easyIdMod.getGSFID();
        }
        return null;
    }

    private String mapDevice(Integer deviceId) {
        switch (deviceId) {
            case DeviceType.TV:
                return "TV";
            case DeviceType.PHONE:
                return "PHONE";
            case DeviceType.PHABLET:
                return "PHABLET";
            case DeviceType.WATCH:
                return "WATCH";
            case DeviceType.TABLET:
                return "TABLET";
        }

        return "UNKNOWN";
    }

    private String mapOrientation(Integer orientationId) {
        switch (orientationId) {
            case Configuration.ORIENTATION_LANDSCAPE:
                return "LANDSCAPE";
            case Configuration.ORIENTATION_PORTRAIT:
                return "PORTRAIT";
        }
        return "UNKNOWN";
    }

    @SuppressWarnings("deprecation")
    private String getIMEI() {
        if (Permission.isGranted(activity.getApplicationContext(), permission.READ_PHONE_STATE)) {
            return easyDeviceMod.getIMEI();
        }
        return null;
    }
}
