package com.weq.studios.sdk.trackingevent.model;

public class TrackingDeviceInfo {

    private String advertisingDeviceId;
    private String deviceId;
    private Platform platform;
    private String osVersion;
    private String brand;
    private String model;
    private Size screenSize;
    private Double batteryLevel;
    private Boolean limitedAdTracking;
    private Boolean jailBroken;
    private TrackingAndroidInfo androidInfo;

    public TrackingDeviceInfo() {
    }

    public TrackingDeviceInfo(
        String advertisingDeviceId,
        String deviceId,
        String osVersion,
        String brand,
        String model,
        Size screenSize,
        Double batteryLevel,
        Boolean limitedAdTracking,
        Boolean jailBroken,
        TrackingAndroidInfo androidInfo
    ) {
        this.advertisingDeviceId = advertisingDeviceId;
        this.deviceId = deviceId;
        this.platform = Platform.ANDROID;
        this.osVersion = osVersion;
        this.brand = brand;
        this.model = model;
        this.screenSize = screenSize;
        this.batteryLevel = batteryLevel;
        this.limitedAdTracking = limitedAdTracking;
        this.jailBroken = jailBroken;
        this.androidInfo = androidInfo;
    }

    public String getAdvertisingDeviceId() {
        return advertisingDeviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public Platform getPlatform() {
        return platform;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public Size getScreenSize() {
        return screenSize;
    }

    public Double getBatteryLevel() {
        return batteryLevel;
    }

    public Boolean getLimitedAdTracking() {
        return limitedAdTracking;
    }

    public Boolean getJailBroken() {
        return jailBroken;
    }

    public TrackingAndroidInfo getAndroidInfo() {
        return androidInfo;
    }
}
