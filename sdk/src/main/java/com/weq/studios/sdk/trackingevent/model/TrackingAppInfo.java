package com.weq.studios.sdk.trackingevent.model;

public class TrackingAppInfo {

    private String appId;
    private String build;
    private String version;
    private String signature;

    public TrackingAppInfo() {
    }

    public TrackingAppInfo(String appId, String build, String version, String signature) {
        this.appId = appId;
        this.build = build;
        this.version = version;
        this.signature = signature;
    }

    public String getAppId() {
        return appId;
    }

    public String getBuild() {
        return build;
    }

    public String getVersion() {
        return version;
    }

    public String getSignature() {
        return signature;
    }
}
