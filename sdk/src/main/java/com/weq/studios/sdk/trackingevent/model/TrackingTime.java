package com.weq.studios.sdk.trackingevent.model;

public class TrackingTime {

    private String clientTime;

    public TrackingTime(String clientTime) {
        this.clientTime = clientTime;
    }

    public String getClientTime() {
        return clientTime;
    }
}
