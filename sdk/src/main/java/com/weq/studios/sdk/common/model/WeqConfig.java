package com.weq.studios.sdk.common.model;

import android.app.Activity;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

public class WeqConfig {

    private Activity activity;
    private String appToken;
    private String gameId;

    public WeqConfig(
        Activity activity,
        String appToken,
        String gameId
    ) {
        this.activity = activity;
        this.appToken = appToken;
        this.gameId = gameId;
    }

    public Activity getActivity() {
        return activity;
    }

    public String getAppToken() {
        return appToken;
    }

    public String getGameId() {
        return gameId;
    }

    public List<String> validate() {
        ArrayList<String> messages = new ArrayList<>();
        if (TextUtils.isEmpty(appToken)) {
            messages.add("App token is not set");
        }

        if (TextUtils.isEmpty(gameId)) {
            messages.add("Game id is not set");
        }

        if (activity == null) {
            messages.add("Activity is not set");
        }

        return messages;
    }
}
