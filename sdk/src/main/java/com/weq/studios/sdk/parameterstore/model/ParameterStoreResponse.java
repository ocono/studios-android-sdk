package com.weq.studios.sdk.parameterstore.model;

import java.util.Collection;

public class ParameterStoreResponse {

    private Collection<ParameterValueResponse> parameters;

    public ParameterStoreResponse(Collection<ParameterValueResponse> parameters) {
        this.parameters = parameters;
    }

    public Collection<ParameterValueResponse> getParameters() {
        return parameters;
    }
}
