package com.weq.studios.sdk.gameuser.model;

import com.weq.studios.sdk.trackingevent.model.TrackingDeviceInfo;

public class GameUserRequest {

    private final TrackingDeviceInfo deviceInfo;

    public GameUserRequest(TrackingDeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public TrackingDeviceInfo getDeviceInfo() {
        return deviceInfo;
    }
}
