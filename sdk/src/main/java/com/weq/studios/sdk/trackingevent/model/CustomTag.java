package com.weq.studios.sdk.trackingevent.model;

public class CustomTag {

    private String name;
    private String value;

    public CustomTag(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
