package com.weq.studios.sdk.parameterstore;

import android.content.ContentValues;
import android.database.Cursor;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.weq.studios.sdk.database.AnalyticsDatabase;
import com.weq.studios.sdk.parameterstore.model.ParameterValue;
import java.util.HashMap;
import java.util.Map;

public class ParameterValuesTable {

    private static final String TABLE_PARAMETER_VALUES = "parameter_values";
    private static final String TABLE_PARAMETER_VALUES_COLUMN_NAME = "name";
    private static final String TABLE_PARAMETER_VALUES_COLUMN_ATTRIBUTES = "attributes";
    private static final String TABLE_PARAMETER_VALUES_COLUMN_TIMESTAMP = "timestamp";

    public static final String SQL_CREATE_TABLE =
        "CREATE TABLE IF NOT EXISTS " + ParameterValuesTable.TABLE_PARAMETER_VALUES + " (" +
            ParameterValuesTable.TABLE_PARAMETER_VALUES_COLUMN_NAME + " TEXT NOT NULL UNIQUE," +
            ParameterValuesTable.TABLE_PARAMETER_VALUES_COLUMN_ATTRIBUTES + " TEXT," +
            ParameterValuesTable.TABLE_PARAMETER_VALUES_COLUMN_TIMESTAMP + " INTEGER)";

    private final AnalyticsDatabase analyticsDatabase;
    private final Gson gson;

    public ParameterValuesTable(AnalyticsDatabase analyticsDatabase, Gson gson) {
        this.analyticsDatabase = analyticsDatabase;
        this.gson = gson;
    }

    public Long upsert(ParameterValue parameterValue) {
        ContentValues values = new ContentValues();
        values.put(TABLE_PARAMETER_VALUES_COLUMN_NAME, parameterValue.getName());
        values.put(TABLE_PARAMETER_VALUES_COLUMN_ATTRIBUTES, gson.toJson(parameterValue));
        values.put(TABLE_PARAMETER_VALUES_COLUMN_TIMESTAMP, System.currentTimeMillis());
        return analyticsDatabase.getWritableDatabase().replace(TABLE_PARAMETER_VALUES, null, values);
    }

    public Map<String, ParameterValue> findAll() {
        String[] projection = {
            TABLE_PARAMETER_VALUES_COLUMN_NAME,
            TABLE_PARAMETER_VALUES_COLUMN_ATTRIBUTES
        };

        Cursor cursor = analyticsDatabase.getWritableDatabase().query(
            TABLE_PARAMETER_VALUES,
            projection,
            null,
            null,
            null,
            null,
            null,
            null
        );

        Map<String, ParameterValue> parameterValues = new HashMap<>();

        try {
            while (cursor.moveToNext()) {

                String name = cursor.getString(
                    cursor.getColumnIndexOrThrow(TABLE_PARAMETER_VALUES_COLUMN_NAME)
                );

                String attributes = cursor.getString(
                    cursor.getColumnIndexOrThrow(TABLE_PARAMETER_VALUES_COLUMN_ATTRIBUTES)
                );

                try {
                    parameterValues.put(name, gson.fromJson(attributes, ParameterValue.class));
                } catch (JsonSyntaxException e) {
                    //delete parameter if attributes could not be unserialized
                    deleteByParameterName(name);
                }
            }
        } finally {
            cursor.close();
        }

        return parameterValues;
    }

    public void deleteByParameterName(String name) {
        String[] selectionArgs = {name};
        analyticsDatabase.getWritableDatabase().delete(
            TABLE_PARAMETER_VALUES,
            TABLE_PARAMETER_VALUES_COLUMN_NAME + " = ?",
            selectionArgs
        );
    }
}
