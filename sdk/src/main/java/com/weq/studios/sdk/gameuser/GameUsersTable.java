package com.weq.studios.sdk.gameuser;

import android.content.ContentValues;
import android.database.Cursor;
import com.weq.studios.sdk.database.AnalyticsDatabase;
import com.weq.studios.sdk.gameuser.model.GameUser;

public class GameUsersTable {

    private static final String TABLE_GAME_USERS_VALUES = "game_users";
    private static final String TABLE_GAME_USERS_COLUMN_ID = "id";
    private static final String TABLE_GAME_USERS_COLUMN_TIMESTAMP = "timestamp";

    public static final String SQL_CREATE_TABLE =
        "CREATE TABLE IF NOT EXISTS " + GameUsersTable.TABLE_GAME_USERS_VALUES + " (" +
            GameUsersTable.TABLE_GAME_USERS_COLUMN_ID + " TEXT NOT NULL UNIQUE," +
            GameUsersTable.TABLE_GAME_USERS_COLUMN_TIMESTAMP + " INTEGER)";

    private final AnalyticsDatabase analyticsDatabase;

    public GameUsersTable(AnalyticsDatabase analyticsDatabase) {
        this.analyticsDatabase = analyticsDatabase;
    }

    public Long upsert(GameUser gameUser) {
        ContentValues values = new ContentValues();
        values.put(TABLE_GAME_USERS_COLUMN_ID, gameUser.getId());
        values.put(TABLE_GAME_USERS_COLUMN_TIMESTAMP, System.currentTimeMillis());
        return analyticsDatabase.getWritableDatabase().replace(TABLE_GAME_USERS_VALUES, null, values);
    }

    public GameUser findOne() {
        String[] projection = {
            TABLE_GAME_USERS_COLUMN_ID
        };

        Cursor cursor = analyticsDatabase.getReadableDatabase().query(
            TABLE_GAME_USERS_VALUES,
            projection,
            null,
            null,
            null,
            null,
            "timestamp DESC",
            "1"
        );

        try {
            while (cursor.moveToNext()) {
                String id = cursor.getString(
                    cursor.getColumnIndexOrThrow(TABLE_GAME_USERS_COLUMN_ID)
                );

                return new GameUser(id);
            }
        } finally {
            cursor.close();
        }
        return null;
    }
}
