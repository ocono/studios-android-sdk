package com.weq.studios.sdk.parameterstore.model;

import java.util.Collection;

public class ParameterStoreRequest {

    private Collection<ParameterValue> parameters;

    public ParameterStoreRequest(Collection<ParameterValue> parameters) {
        this.parameters = parameters;
    }
}
