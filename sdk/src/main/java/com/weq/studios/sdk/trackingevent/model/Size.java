package com.weq.studios.sdk.trackingevent.model;

public class Size {

    private Integer width;
    private Integer height;

    public Size() {
    }

    public Size(Integer width, Integer height) {
        this.width = width;
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }
}
