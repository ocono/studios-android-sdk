package com.weq.studios.sdk.parameterstore;

import com.weq.studios.sdk.common.Logger;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.parameterstore.model.ParameterValue;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ParameterStoreRepository {

    private static final String LOG_TAG = "Weq_parameter_value_repository";

    private final ParameterValuesTable parameterValuesTable;
    private final TrackerState trackerState;
    private final ExecutorService thread = Executors.newSingleThreadExecutor();
    private final Map<String, ParameterValue> parameterCache = new HashMap<>();

    public ParameterStoreRepository(ParameterValuesTable parameterValuesTable, TrackerState trackerState) {
        this.parameterValuesTable = parameterValuesTable;
        this.trackerState = trackerState;

        refreshCache();
    }

    public void registerParameterValue(final ParameterValue parameterValue) {
        if (!trackerState.isTrackingEnabled() || parameterValue.getName().isEmpty()) {
            return;
        }

        thread.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronized (parameterCache) {
                        final String name = parameterValue.getName();
                        //Keep old evaluated value, in case its stale then next evaluation will update
                        if (parameterCache.containsKey(name) && parameterCache.get(name) != null) {
                            parameterValue.setValue(parameterCache.get(name).getValue());
                        }

                        if (parameterValuesTable.upsert(parameterValue) == -1) {
                            Logger.debug(LOG_TAG, String.format("Failed to save parameter %s", name));
                            return;
                        }
                        parameterCache.put(name, parameterValue);
                    }
                } catch (Throwable e) {
                    Logger.debug(LOG_TAG, String.format("An error occurred while saving parameter value %s", e.getMessage()));
                }
            }
        });
    }

    public String getParameterValue(String name) {
        synchronized (parameterCache) {
            if (!parameterCache.containsKey(name)) {
                return null;
            }

            ParameterValue parameterValue = parameterCache.get(name);
            if (parameterValue == null) {
                return null;
            }

            return parameterValue.getValue() != null ? parameterValue.getValue() : parameterValue.getDefaultValue();
        }
    }

    public void refreshCache() {
        thread.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (parameterCache) {
                    parameterCache.clear();
                    parameterCache.putAll(parameterValuesTable.findAll());
                }
            }
        });
    }

    public Map<String, ParameterValue> getCache() {
        return this.parameterCache;
    }

    public Long upsert(ParameterValue parameterValue) {
        return parameterValuesTable.upsert(parameterValue);
    }
}
