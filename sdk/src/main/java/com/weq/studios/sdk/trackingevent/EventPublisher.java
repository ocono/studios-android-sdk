package com.weq.studios.sdk.trackingevent;

import android.util.Log;
import com.google.gson.Gson;
import com.weq.studios.sdk.common.DateTime;
import com.weq.studios.sdk.common.HttpClient;
import com.weq.studios.sdk.common.HttpClient.HttpResponse;
import com.weq.studios.sdk.common.Logger;
import com.weq.studios.sdk.common.Network;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.trackingevent.model.TrackingEvent;
import com.weq.studios.sdk.trackingevent.model.TrackingEventBatch;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class EventPublisher {

    private static final String LOG_TAG = "Weq_event_publisher";
    private static final Integer limitOfFailedAttempts = 30;
    private static final Long delayEventPublisherInSeconds = 10L;
    private static final String EVENT_URI_PATTERN = "/event";

    private final EventRepository eventRepository;
    private final TrackerState trackerState;
    private final HttpClient httpClient;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private final Gson gson = new Gson();
    private final AtomicInteger failedAttempts = new AtomicInteger(0);

    public EventPublisher(
        EventRepository eventRepository,
        TrackerState trackerState,
        HttpClient httpClient
    ) {
        this.eventRepository = eventRepository;
        this.trackerState = trackerState;
        this.httpClient = httpClient;
    }

    public void start() {
        failedAttempts.set(0);
        Logger.debug(LOG_TAG, "Started");

        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    if (shouldStop()) {
                        Logger.debug(LOG_TAG, "Stopping publisher. Too many failed attempts");
                        scheduler.shutdown();
                        return;
                    }

                    if (shouldSkip()) {
                        return;
                    }

                    Map<String, TrackingEvent> events = eventRepository.getEvents(
                        trackerState.getSdkConfiguration().getEventFlushBatchSize()
                    );

                    if (events.isEmpty()) {
                        return;
                    }

                    setGameUserId(events);
                    sendEvents(events);
                } catch (Exception e) {
                    Log.e(LOG_TAG, "An error occurred while publishing tracking event", e);
                    failedAttempts.getAndIncrement();
                }
            }
        }, delayEventPublisherInSeconds, trackerState.getSdkConfiguration().getEventFlushFrequency(), TimeUnit.SECONDS);
    }

    private boolean shouldStop() {
        return failedAttempts.get() >= limitOfFailedAttempts;
    }

    private boolean shouldSkip() {
        if (trackerState.getSession().getUserId() == null) {
            return true;
        }

        if (!trackerState.isTrackingEnabled()) {
            return true;
        }

        return !isConnected();
    }

    public boolean isConnected() {
        return Network.isNetworkAvailable(trackerState.getWeQConfig().getActivity().getApplicationContext());
    }

    private void setGameUserId(final Map<String, TrackingEvent> events) {
        //Game user may be assigned later, after events were registered. Update events with user id before sending.
        for (TrackingEvent event : events.values()) {
            event.getSession().setGameUserId(trackerState.getSession().getUserId());
        }
    }

    private void sendEvents(final Map<String, TrackingEvent> events) {
        Logger.debug(LOG_TAG, String.format(Locale.getDefault(), "Sending event batch request with '%d' events. ", events.size()));

        TrackingEventBatch eventBatch = new TrackingEventBatch(new ArrayList<>(events.values()), DateTime.now());
        HttpResponse response = httpClient.post(
            trackerState.getSdkConfiguration().getEventIngressHostname() + EVENT_URI_PATTERN,
            gson.toJson(eventBatch)
        );

        if (response == null) {
            Logger.debug(LOG_TAG, "An error occurred while sending event request");
            return;
        }

        if (response.isSuccess()) {
            eventRepository.clearByIds(events.keySet());
            Logger.debug(LOG_TAG, String.format(Locale.getDefault(), "Event batch request successfully sent. Received '%d' status code", response.getStatusCode()));
            return;
        }

        failedAttempts.getAndIncrement();
        Logger.debug(LOG_TAG, String.format(Locale.getDefault(), "Failed to sent event batch request. Received '%d' status code", response.getStatusCode()));
    }
}
