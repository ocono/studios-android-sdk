package com.weq.studios.sdk;

import static com.weq.studios.sdk.trackingevent.EventCoder.*;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.weq.studios.sdk.common.HttpClient;
import com.weq.studios.sdk.common.Logger;
import com.weq.studios.sdk.common.model.SdkConfiguration;
import com.weq.studios.sdk.database.AnalyticsDatabase;
import com.weq.studios.sdk.gameuser.GameUserResolver;
import com.weq.studios.sdk.gameuser.GameUsersTable;
import com.weq.studios.sdk.parameterstore.ParameterStoreUpdater;
import com.weq.studios.sdk.trackingevent.EventsTable;
import com.weq.studios.sdk.parameterstore.ParameterValuesTable;
import com.weq.studios.sdk.trackingevent.model.EventDefinition;
import com.weq.studios.sdk.trackingevent.model.EventValue;
import com.weq.studios.sdk.common.model.Session;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.common.model.WeqConfig;
import com.weq.studios.sdk.trackingevent.model.WeqEvent;

import com.weq.studios.sdk.parameterstore.ParameterStoreRepository;
import com.weq.studios.sdk.parameterstore.model.ParameterValue;
import com.weq.studios.sdk.trackingevent.EventCollector;
import com.weq.studios.sdk.trackingevent.EventPublisher;
import com.weq.studios.sdk.trackingevent.EventRepository;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class WeqAnalytics {

    private static final String LOG_TAG = "Weq_api";

    private static EventRepository eventRepository;
    private static ParameterStoreRepository parameterStoreRepository;
    private static EventCollector collector;
    private static TrackerState trackerState;
    private static AtomicBoolean started = new AtomicBoolean(false);
    private static Boolean initialized = false;
    private static ExecutorService thread = Executors.newSingleThreadExecutor();
    private static HttpClient httpClient;

    public static void configure(final WeqConfig weqConfig) {
        List<String> validationMessages = weqConfig.validate();
        if (!validationMessages.isEmpty()) {
            for (String message : validationMessages) {
                Logger.debug(LOG_TAG, message);
            }
            return;
        }

        thread.submit(new Runnable() {
            @Override
            public void run() {
                if (started.compareAndSet(false, true)) {
                    Context applicationContext = weqConfig.getActivity().getApplicationContext();
                    SdkConfiguration sdkConfiguration = new SdkConfiguration(applicationContext);

                    trackerState = new TrackerState(weqConfig, sdkConfiguration, new Session());
                    collector = new EventCollector(trackerState);

                    Gson gson = new Gson();
                    if (httpClient == null) {
                        httpClient = new HttpClient(weqConfig);
                    }

                    AnalyticsDatabase database = new AnalyticsDatabase(applicationContext);

                    EventsTable eventsTable = new EventsTable(database, gson);
                    ParameterValuesTable parameterValuesTable = new ParameterValuesTable(database, gson);
                    GameUsersTable gameUsersTable = new GameUsersTable(database);

                    parameterStoreRepository = new ParameterStoreRepository(parameterValuesTable, trackerState);
                    eventRepository = new EventRepository(eventsTable, trackerState);

                    new GameUserResolver(gameUsersTable, trackerState, collector, httpClient, gson).resolve();
                    new ParameterStoreUpdater(parameterStoreRepository, trackerState, httpClient, gson).start();
                    new EventPublisher(eventRepository, trackerState, httpClient).start();

                    initialized = true;
                } else {
                    Logger.debug(LOG_TAG, "Already configured");
                }
            }
        });
    }

    public static void event(final WeqEvent weQEvent) {
        if (!initialized) {
            Log.e(LOG_TAG, "Configure tracking first. Call method WeqAnalytics.configure.");
            return;
        }

        thread.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    eventRepository.addEvent(collector.createFrom(weQEvent));
                } catch (Throwable e) {
                    Log.e(LOG_TAG, "An error occurred while creating tracking event", e);
                }
            }
        });
    }

    /**
     * @param tags each value in tags array is JSON serialized key value pair of CustomTag [{"value" => "1"}, {"value" => "2"}]
     * @param value1 each value is JSON serialized key value pair of CustomValue {"value" => 1} same with other value arguments
     */
    public static void event(
        String category,
        String name,
        String[] tags,
        String value1,
        String value2,
        String value3,
        String value4,
        String value5
    ) {

        WeqAnalytics.event(new WeqEvent(
            new EventDefinition(
                category,
                name,
                unserializeTags(tags)
            ),
            new EventValue(
                unserializeValue(value1),
                unserializeValue(value2),
                unserializeValue(value3),
                unserializeValue(value4),
                unserializeValue(value5)
            )
        ));
    }

    public static void configure(Activity activity, String appToken, String gameId) {
        WeqAnalytics.configure(new WeqConfig(activity, appToken, gameId));
    }

    public static void registerParameter(String name, String type, String defaultValue, String description) {
        parameterStoreRepository.registerParameterValue(new ParameterValue(
            name,
            type,
            description,
            defaultValue
        ));
    }

    public static String getParameter(String name) {
        return parameterStoreRepository.getParameterValue(name);
    }

    /**
     * Disable tracking, wont store any data to local storage or send events to WeQ event API
     */
    public static void disableTracking() {
        trackerState.setTrackingEnabled(false);
        Logger.debug(LOG_TAG, "Tracking disabled");
    }

    /**
     * Resume tracking
     */
    public static void enableTracking() {
        trackerState.setTrackingEnabled(true);
        Logger.debug(LOG_TAG, "Tracking resumed");
    }

    /**
     * Enable/Disable debug logger output
     */
    public static void debug(boolean enabled) {
        Logger.setIsEnabled(enabled);
    }

    static void setHttpClient(HttpClient httpClient) {
        WeqAnalytics.httpClient = httpClient;
    }
}
