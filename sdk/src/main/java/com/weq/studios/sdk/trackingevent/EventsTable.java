package com.weq.studios.sdk.trackingevent;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.weq.studios.sdk.database.AnalyticsDatabase;
import com.weq.studios.sdk.trackingevent.model.TrackingEvent;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class EventsTable {

    private static final String TABLE_EVENTS = "events";
    private static final String TABLE_EVENTS_COLUMN_ID = "id";
    private static final String TABLE_EVENTS_COLUMN_ATTRIBUTES = "attributes";
    private static final String TABLE_EVENTS_COLUMN_TIMESTAMP = "timestamp";

    public static final String SQL_CREATE_TABLE =
        "CREATE TABLE IF NOT EXISTS " + EventsTable.TABLE_EVENTS + " (" +
            EventsTable.TABLE_EVENTS_COLUMN_ID + " TEXT," +
            EventsTable.TABLE_EVENTS_COLUMN_ATTRIBUTES + " TEXT," +
            EventsTable.TABLE_EVENTS_COLUMN_TIMESTAMP + " INTEGER)";

    private final AnalyticsDatabase analyticsDatabase;
    private final Gson gson;

    public EventsTable(AnalyticsDatabase analyticsDatabase, Gson gson) {
        this.analyticsDatabase = analyticsDatabase;
        this.gson = gson;
    }

    public void insert(TrackingEvent event) {
        ContentValues values = new ContentValues();
        values.put(TABLE_EVENTS_COLUMN_ID, event.getId());
        values.put(TABLE_EVENTS_COLUMN_ATTRIBUTES, gson.toJson(event));
        values.put(TABLE_EVENTS_COLUMN_TIMESTAMP, System.currentTimeMillis());
        analyticsDatabase.getWritableDatabase().insert(TABLE_EVENTS, null, values);
    }

    public Map<String, TrackingEvent> findAll(Integer limit) {
        String[] projection = {
            TABLE_EVENTS_COLUMN_ID,
            TABLE_EVENTS_COLUMN_ATTRIBUTES
        };

        Cursor cursor = analyticsDatabase.getWritableDatabase().query(
            TABLE_EVENTS,
            projection,
            null,
            null,
            null,
            null,
            null,
            String.valueOf(limit)
        );

        Map<String, TrackingEvent> events = new HashMap<>();
        Set<String> brokenIds = new HashSet<>();

        try {
            while (cursor.moveToNext()) {
                String id = cursor.getString(
                    cursor.getColumnIndexOrThrow(TABLE_EVENTS_COLUMN_ID)
                );

                String attributes = cursor.getString(
                    cursor.getColumnIndexOrThrow(TABLE_EVENTS_COLUMN_ATTRIBUTES)
                );
                try {
                    events.put(id, gson.fromJson(attributes, TrackingEvent.class));
                } catch (JsonSyntaxException e) {
                    brokenIds.add(id);
                }
            }
        } finally {
            cursor.close();
        }

        if (!brokenIds.isEmpty()) {
            clearByIds(brokenIds);
        }

        return events;
    }

    public Integer clearByIds(final Set<String> ids) {
        return analyticsDatabase.getWritableDatabase().delete(TABLE_EVENTS, createInClause(ids), null);
    }

    public Long count() {
        return DatabaseUtils.queryNumEntries(analyticsDatabase.getWritableDatabase(), TABLE_EVENTS);
    }

    public Integer clearExpired(Integer numberOfOldEvents) {
        return analyticsDatabase.getWritableDatabase().delete(
            TABLE_EVENTS,
            String.format(
                Locale.getDefault(),
                "id IN (SELECT id FROM events ORDER BY timestamp ASC LIMIT %d)",
                numberOfOldEvents
            ),
            null
        );
    }

    private String createInClause(Set<String> ids) {
        StringBuilder idString = new StringBuilder();
        int i = 0;
        for (String id : ids) {
            idString.append("\"")
                .append(id)
                .append("\"");

            if (i++ < ids.size() - 1) {
                idString.append(",");
            }
        }
        return TABLE_EVENTS_COLUMN_ID + " IN (" + idString + ")";
    }
}
