package com.weq.studios.sdk.trackingevent.model;

import github.nisrulz.easydeviceinfo.base.NetworkType;

public enum ConnectionType {
    WIFI,
    CELLULAR_2_G,
    CELLULAR_3_G,
    CELLULAR_4_G,
    CELLULAR_5_G,
    UNKNOWN;


    public static ConnectionType getConnectionTypeById(Integer id) {
        switch (id) {
            case NetworkType.WIFI_WIFIMAX:
                return WIFI;
            case NetworkType.CELLULAR_2G:
                return ConnectionType.CELLULAR_2_G;
            case NetworkType.CELLULAR_3G:
                return ConnectionType.CELLULAR_3_G;
            case NetworkType.CELLULAR_4G:
                return ConnectionType.CELLULAR_4_G;
            default:
                return null;
        }

    }
}
