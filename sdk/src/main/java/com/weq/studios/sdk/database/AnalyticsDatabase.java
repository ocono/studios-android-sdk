package com.weq.studios.sdk.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.weq.studios.sdk.gameuser.GameUsersTable;
import com.weq.studios.sdk.parameterstore.ParameterValuesTable;
import com.weq.studios.sdk.trackingevent.EventsTable;

public class AnalyticsDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "analytics.db";

    public AnalyticsDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            super.setWriteAheadLoggingEnabled(true);
        }
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(EventsTable.SQL_CREATE_TABLE);
        db.execSQL(ParameterValuesTable.SQL_CREATE_TABLE);
        db.execSQL(GameUsersTable.SQL_CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
}
