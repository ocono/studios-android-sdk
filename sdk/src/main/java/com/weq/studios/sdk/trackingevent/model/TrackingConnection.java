package com.weq.studios.sdk.trackingevent.model;

public class TrackingConnection {

    private String ip;
    private ConnectionType type;

    public TrackingConnection() {
    }

    public TrackingConnection(String ip, ConnectionType type) {
        this.ip = ip;
        this.type = type;
    }

    public String getIp() {
        return ip;
    }

    public ConnectionType getType() {
        return type;
    }
}
