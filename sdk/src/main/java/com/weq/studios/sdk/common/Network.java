package com.weq.studios.sdk.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static android.Manifest.*;

public class Network {

    public static boolean isNetworkAvailable(Context context) {
        if (!Permission.isGranted(context, permission.INTERNET) ||
            !Permission.isGranted(context, permission.ACCESS_NETWORK_STATE)
        ) {
            return false;
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
