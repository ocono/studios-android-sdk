package com.weq.studios.sdk.parameterstore;

import android.util.Log;
import com.google.gson.Gson;
import com.weq.studios.sdk.common.HttpClient;
import com.weq.studios.sdk.common.HttpClient.HttpResponse;
import com.weq.studios.sdk.common.Logger;
import com.weq.studios.sdk.common.Network;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.parameterstore.model.ParameterStoreRequest;
import com.weq.studios.sdk.parameterstore.model.ParameterStoreResponse;
import com.weq.studios.sdk.parameterstore.model.ParameterValue;
import com.weq.studios.sdk.parameterstore.model.ParameterValueResponse;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ParameterStoreUpdater {

    private static final String LOG_TAG = "Weq_param_updater";
    private static final String PARAMETER_STORE_URI_PATTERN = "/game/%s/user/%s/parameter-store";
    private static final Long delayParameterUpdaterInSeconds = 5L;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final ParameterStoreRepository parameterStoreRepository;
    private final TrackerState trackerState;
    private final HttpClient httpClient;
    private final Gson gson;

    public ParameterStoreUpdater(
        ParameterStoreRepository parameterStoreRepository,
        TrackerState trackerState,
        HttpClient httpClient,
        Gson gson
    ) {
        this.parameterStoreRepository = parameterStoreRepository;
        this.trackerState = trackerState;
        this.httpClient = httpClient;
        this.gson = gson;
    }

    public void start() {
        Logger.debug(LOG_TAG, "Started");
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    if (trackerState.getSession().getUserId() == null ||
                        parameterStoreRepository.getCache().isEmpty()) {
                        return;
                    }

                    if (!isConnected()) {
                        return;
                    }

                    HttpResponse response = httpClient.post(
                        createParameterStoreApiEndpointUri(),
                        gson.toJson(new ParameterStoreRequest(parameterStoreRepository.getCache().values()))
                    );

                    if (response == null) {
                        return;
                    }

                    ParameterStoreResponse parameterStoreResponse = gson.fromJson(response.getData(), ParameterStoreResponse.class);
                    processApiResponse(parameterStoreResponse);

                    parameterStoreRepository.refreshCache();
                } catch (Exception e) {
                    Log.d(LOG_TAG, "An error occurred while executing parameter update", e);
                }
            }
        }, delayParameterUpdaterInSeconds, trackerState.getSdkConfiguration().getParameterStoreUpdateFrequencyInSeconds(), TimeUnit.SECONDS);
    }

    private void processApiResponse(ParameterStoreResponse parameterStoreResponse) {
        Map<String, ParameterValue> parameterCache = parameterStoreRepository.getCache();
        for (ParameterValueResponse parameterResponseValue : parameterStoreResponse.getParameters()) {
            if (!parameterCache.containsKey(parameterResponseValue.getName())) {
                continue;
            }

            ParameterValue parameterValue = parameterCache.get(parameterResponseValue.getName());
            if (parameterValue == null) {
                continue;
            }

            parameterValue.setValue(parameterResponseValue.getValue());
            if (parameterStoreRepository.upsert(parameterValue) == -1) {
                Logger.debug(LOG_TAG, String.format("Failed to save parameter %s", parameterResponseValue.getName()));
            }
        }
    }

    private boolean isConnected() {
        return Network.isNetworkAvailable(trackerState.getWeQConfig().getActivity().getApplicationContext());
    }

    private String createParameterStoreApiEndpointUri() {
        return String.format(
            trackerState.getSdkConfiguration().getEventIngressHostname() + PARAMETER_STORE_URI_PATTERN,
            trackerState.getWeQConfig().getGameId(),
            trackerState.getSession().getUserId()
        );
    }

}
