package com.weq.studios.sdk.trackingevent.model;

import com.weq.studios.sdk.trackingevent.model.EventDefinition;
import com.weq.studios.sdk.trackingevent.model.EventValue;

public class WeqEvent {

    private EventDefinition type;
    private EventValue value;

    public WeqEvent(EventDefinition eventDefinition, EventValue eventValue) {
        this.type = eventDefinition;
        this.value = eventValue;
    }

    public WeqEvent(EventDefinition eventDefinition) {
        this.type = eventDefinition;
        this.value = new EventValue();
    }

    public EventDefinition getType() {
        return type;
    }

    public EventValue getValue() {
        return value;
    }
}
