package com.weq.studios.sdk.parameterstore.model;

public class ParameterValueResponse {

    private String name;
    private String value;

    public ParameterValueResponse(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
