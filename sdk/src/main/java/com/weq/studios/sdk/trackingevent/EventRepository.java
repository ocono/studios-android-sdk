package com.weq.studios.sdk.trackingevent;

import com.weq.studios.sdk.common.Logger;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.trackingevent.model.TrackingEvent;

import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class EventRepository {

    private static final String LOG_TAG = "Weq_event_repository";

    private final EventsTable eventsTable;
    private final TrackerState trackerState;
    private final ExecutorService thread = Executors.newSingleThreadExecutor();
    private final AtomicLong numberOfEventsInStorage = new AtomicLong(0);

    public EventRepository(EventsTable eventsTable, TrackerState trackerState) {
        this.eventsTable = eventsTable;
        this.trackerState = trackerState;
        countEventsFromDatabase();
    }

    public void addEvent(final TrackingEvent trackingEvent) {
        if (!trackerState.isTrackingEnabled()) {
            return;
        }

        thread.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    if (numberOfEventsInStorage.get() >= trackerState.getSdkConfiguration().getEventStorageSizeLimit()) {
                        Logger.debug(
                            LOG_TAG,
                            String.format(
                                Locale.getDefault(),
                                "Reached limit of events that can be stored to internal database. Clearing '%d' oldest events",
                                trackerState.getSdkConfiguration().getEventStorageDeleteOldest()
                            )
                        );
                        clearOldEvents();
                    }

                    Logger.debug(LOG_TAG, "Saving event");
                    eventsTable.insert(trackingEvent);
                    incrementEventCounter();
                } catch (Throwable e) {
                    Logger.debug(LOG_TAG, String.format("An error occurred while saving tracking event %s", e.getMessage()));
                }
            }
        });
    }

    public void clearByIds(final Set<String> ids) {
        thread.submit(new Runnable() {
            @Override
            public void run() {
                int affected = eventsTable.clearByIds(ids);
                Logger.debug(LOG_TAG, String.format(Locale.getDefault(), "Deleted '%d' events", affected));
                if (affected > 0) {
                    countEventsFromDatabase();
                }
            }
        });
    }

    public Map<String, TrackingEvent> getEvents(Integer limit) {
        Map<String, TrackingEvent> events = eventsTable.findAll(limit);
        countEventsFromDatabase();
        return events;
    }

    private void countEventsFromDatabase() {
        numberOfEventsInStorage.set(eventsTable.count());
    }

    private void clearOldEvents() {
        int affected = eventsTable.clearExpired(trackerState.getSdkConfiguration().getEventStorageDeleteOldest());
        Logger.debug(LOG_TAG, String.format(Locale.getDefault(), "Cleared '%d' old events", affected));
        if (affected > 0) {
            countEventsFromDatabase();
        }
    }

    private void incrementEventCounter() {
        if (numberOfEventsInStorage.get() == 0) {
            countEventsFromDatabase();
            return;
        }
        //Try avoid count from database for each registered event
        numberOfEventsInStorage.getAndIncrement();
        Logger.debug(LOG_TAG, String.format(Locale.getDefault(), "Number of events in storage '%d'.", numberOfEventsInStorage.get()));
    }

}

