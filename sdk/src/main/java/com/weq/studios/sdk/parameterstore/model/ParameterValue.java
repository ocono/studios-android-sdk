package com.weq.studios.sdk.parameterstore.model;

public class ParameterValue {

    private String name;
    private String type;
    private String description;
    private String defaultValue;
    private String value;

    public ParameterValue(String name, String type, String description, String defaultValue) {
        this.name = name;
        this.type = type;
        this.description = description;
        this.defaultValue = defaultValue;
    }

    public ParameterValue(String name, String type, String description, String defaultValue, String value) {
        this.name = name;
        this.type = type;
        this.description = description;
        this.defaultValue = defaultValue;
        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getValue() {
        return value;
    }
}
