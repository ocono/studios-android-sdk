package com.weq.studios.sdk.trackingevent.model;

public class EventValue {

    private CustomValue value1;
    private CustomValue value2;
    private CustomValue value3;
    private CustomValue value4;
    private CustomValue value5;

    public EventValue(CustomValue value1, CustomValue value2, CustomValue value3, CustomValue value4, CustomValue value5) {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
        this.value5 = value5;
    }

    public EventValue() {

    }

}
