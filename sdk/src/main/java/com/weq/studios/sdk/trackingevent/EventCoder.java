package com.weq.studios.sdk.trackingevent;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.weq.studios.sdk.common.Logger;
import com.weq.studios.sdk.trackingevent.model.CustomTag;
import com.weq.studios.sdk.trackingevent.model.CustomValue;
import java.util.ArrayList;
import java.util.List;

public class EventCoder {

    private static final String LOG_TAG = "Weq_event_coder";
    private static final Gson gson = new Gson();

    public static List<CustomTag> unserializeTags(String[] tags)  {
        List<CustomTag> internalTags = new ArrayList<>();
        for (String tag : tags) {
            try {
                internalTags.add(gson.fromJson(tag, CustomTag.class));
            } catch (JsonSyntaxException e) {
                Logger.debug(LOG_TAG, String.format("An error occurred while decoding tag value %s - %s", tag, e.getMessage()));
            }
        }
        return internalTags;
    }

    public static CustomValue unserializeValue(String value) {
        if (value == null || value.trim().isEmpty()) {
            return null;
        }
        try {
            return gson.fromJson(value, CustomValue.class);
        } catch (JsonSyntaxException e) {
            Logger.debug(LOG_TAG, String.format("An error occurred while decoding value %s - %s", value, e.getMessage()));
            return null;
        }
    }
}
