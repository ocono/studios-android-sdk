package com.weq.studios.sdk.common.model;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SdkConfiguration {

    public static String PROPERTY_WEQ_SDK_EVENT_INGRESS_HOSTNAME = "WEQ_SDK_EVENT_INGRESS_HOSTNAME";
    public static String PROPERTY_WEQ_SDK_EVENT_FLUSH_FREQUENCY_IN_SECONDS = "WEQ_SDK_EVENT_FLUSH_FREQUENCY_IN_SECONDS";
    public static String PROPERTY_WEQ_SDK_EVENT_FLUSH_BATCH_SIZE = "WEQ_SDK_EVENT_FLUSH_BATCH_SIZE";
    public static String PROPERTY_WEQ_SDK_EVENT_STORAGE_SIZE_LIMIT = "WEQ_SDK_EVENT_STORAGE_SIZE_LIMIT";
    public static String PROPERTY_WEQ_SDK_EVENT_STORAGE_DELETE_OLDEST = "WEQ_SDK_EVENT_STORAGE_DELETE_OLDEST";
    public static String PROPERTY_WEQ_SDK_PARAMETER_STORE_UPDATE_FREQUENCY_IN_SECONDS = "WEQ_SDK_PARAMETER_STORE_UPDATE_FREQUENCY_IN_SECONDS";

    private static final String LOG_TAG = "Weq_sdk_configuration";

    private final Properties properties;

    public SdkConfiguration(Context context) {
        this.properties = new Properties();

        try {
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open("sdk.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            Log.e(LOG_TAG, "An error occurred while reading sdk.properties file", e);
        }
    }

    public SdkConfiguration(Properties properties) {
        this.properties = properties;
    }

    public String getEventIngressHostname() {
        return properties.getProperty(PROPERTY_WEQ_SDK_EVENT_INGRESS_HOSTNAME);
    }

    public Long getEventFlushFrequency() {
        return Long.valueOf(properties.getProperty(PROPERTY_WEQ_SDK_EVENT_FLUSH_FREQUENCY_IN_SECONDS));
    }

    public Integer getEventFlushBatchSize() {
        return Integer.valueOf(properties.getProperty(PROPERTY_WEQ_SDK_EVENT_FLUSH_BATCH_SIZE));
    }

    public Integer getEventStorageSizeLimit() {
        return Integer.valueOf(properties.getProperty(PROPERTY_WEQ_SDK_EVENT_STORAGE_SIZE_LIMIT));
    }

    public Integer getEventStorageDeleteOldest() {
        return Integer.valueOf(properties.getProperty(PROPERTY_WEQ_SDK_EVENT_STORAGE_DELETE_OLDEST));
    }

    public Integer getParameterStoreUpdateFrequencyInSeconds() {
        return Integer.valueOf(properties.getProperty(PROPERTY_WEQ_SDK_PARAMETER_STORE_UPDATE_FREQUENCY_IN_SECONDS));
    }
}
