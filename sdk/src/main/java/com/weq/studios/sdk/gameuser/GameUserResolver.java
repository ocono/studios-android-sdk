package com.weq.studios.sdk.gameuser;

import android.util.Log;
import com.google.gson.Gson;
import com.weq.studios.sdk.common.HttpClient;
import com.weq.studios.sdk.common.HttpClient.HttpResponse;
import com.weq.studios.sdk.common.Logger;
import com.weq.studios.sdk.common.Network;
import com.weq.studios.sdk.common.model.TrackerState;
import com.weq.studios.sdk.gameuser.model.GameUser;
import com.weq.studios.sdk.gameuser.model.GameUserRequest;
import com.weq.studios.sdk.gameuser.model.GameUserResponse;
import com.weq.studios.sdk.trackingevent.EventCollector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameUserResolver {

    private static final String GAME_USER_URI_PATTERN = "/game/%s/user";

    private static final String LOG_TAG = "Weq_game_user_resolver";
    private static final Integer allowedNumberOfAttempts = 5;
    private static final Integer gameUserResolutionAttemptFrequencyInSeconds = 10;
    private static Integer numberOfAttempts = 0;

    private final GameUsersTable gameUsersTable;
    private final TrackerState trackerState;
    private final EventCollector eventCollector;
    private final HttpClient httpClient;
    private final Gson gson;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public GameUserResolver(
        GameUsersTable gameUsersTable,
        TrackerState trackerState,
        EventCollector eventCollector,
        HttpClient httpClient,
        Gson gson
    ) {
        this.gameUsersTable = gameUsersTable;
        this.trackerState = trackerState;
        this.eventCollector = eventCollector;
        this.httpClient = httpClient;
        this.gson = gson;
    }

    public void resolve() {
        Logger.debug(LOG_TAG, "Resolving game user");
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!shouldRun()) {
                        scheduler.shutdown();
                    }

                    GameUser gameUser = gameUsersTable.findOne();
                    if (gameUser != null) {
                        trackerState.getSession().setUserId(gameUser.getId());
                        return;
                    }

                    if (!isConnected()) {
                        return;
                    }

                    GameUserResponse gameUserResponse = fetchGameUserFromAPI();
                    if (gameUserResponse != null) {
                        if (gameUsersTable.upsert(new GameUser(gameUserResponse.getUserId())) != -1) {
                            trackerState.getSession().setUserId(gameUserResponse.getUserId());
                        }
                    }
                    numberOfAttempts++;
                } catch (Exception e) {
                    Log.d(LOG_TAG, "An error occurred while executing game user resolver", e);
                }
            }
        }, 5, gameUserResolutionAttemptFrequencyInSeconds, TimeUnit.SECONDS);
    }

    private boolean shouldRun() {
        if (trackerState.getSession().getUserId() != null) {
            Logger.debug(LOG_TAG, "Game user was assigned");
            return false;
        }

        if (numberOfAttempts >= allowedNumberOfAttempts) {
            Logger.debug(LOG_TAG, "Failed to get user after number of retries");
            return false;
        }

        return true;
    }

    private boolean isConnected() {
        return Network.isNetworkAvailable(trackerState.getWeQConfig().getActivity().getApplicationContext());
    }

    private GameUserResponse fetchGameUserFromAPI() {
        GameUserRequest gameUserRequest = new GameUserRequest(
            eventCollector.createTrackingDeviceInfo()
        );

        HttpResponse response = httpClient.post(
            createGameUserApiEndpointUri(),
            gson.toJson(gameUserRequest)
        );

        if (response == null) {
            return null;
        }

        return gson.fromJson(response.getData(), GameUserResponse.class);
    }

    private String createGameUserApiEndpointUri() {
        return String.format(
            trackerState.getSdkConfiguration().getEventIngressHostname() + GAME_USER_URI_PATTERN,
            trackerState.getWeQConfig().getGameId()
        );
    }
}
