package com.weq.studios.sdk.trackingevent.model;

public class TrackingSession {

    private String id;
    private String advertisingDeviceId;
    private String gameUserId;
    private String startTime;
    private String currentTime;
    private String deviceId;
    private String gameCenterId;

    public TrackingSession() {
    }

    public TrackingSession(
        String id,
        String advertisingDeviceId,
        String gameUserId,
        String startTime,
        String currentTime,
        String deviceId,
        String gameCenterId
    ) {
        this.id = id;
        this.advertisingDeviceId = advertisingDeviceId;
        this.gameUserId = gameUserId;
        this.startTime = startTime;
        this.currentTime = currentTime;
        this.deviceId = deviceId;
        this.gameCenterId = gameCenterId;
    }


    public String getId() {
        return id;
    }

    public void setGameUserId(String gameUserId) {
        this.gameUserId = gameUserId;
    }

    public String getAdvertisingDeviceId() {
        return advertisingDeviceId;
    }

    public String getGameUserId() {
        return gameUserId;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getGameCenterId() {
        return gameCenterId;
    }
}
