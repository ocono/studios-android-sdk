package com.weq.studios.sdk.gameuser.model;

public class GameUser {

    private final String id;

    public GameUser(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
