package com.weq.studios.sdk.trackingevent.model;

import java.util.List;

public class TrackingEventBatch {

    private List<TrackingEvent> events;
    private String time;

    public TrackingEventBatch() {
    }

    public TrackingEventBatch(List<TrackingEvent> events, String time) {
        this.events = events;
        this.time = time;
    }

    public List<TrackingEvent> getEvents() {
        return events;
    }

    public String getTime() {
        return time;
    }
}
