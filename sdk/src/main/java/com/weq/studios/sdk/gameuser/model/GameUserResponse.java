package com.weq.studios.sdk.gameuser.model;

public class GameUserResponse {

    private final String advertiserDeviceId;
    private final String userId;

    public GameUserResponse(String advertiserDeviceId, String userId) {
        this.advertiserDeviceId = advertiserDeviceId;
        this.userId = userId;
    }

    public String getAdvertiserDeviceId() {
        return advertiserDeviceId;
    }

    public String getUserId() {
        return userId;
    }
}
