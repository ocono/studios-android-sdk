package com.weq.studios.sdk.common.model;

public class TrackerState {

    private final WeqConfig weQConfig;
    private final SdkConfiguration sdkConfiguration;
    private final Session session;
    private boolean trackingEnabled = true;

    public TrackerState(WeqConfig weQConfig, SdkConfiguration sdkConfiguration, Session session) {
        this.weQConfig = weQConfig;
        this.sdkConfiguration = sdkConfiguration;
        this.session = session;
    }

    public void setTrackingEnabled(boolean trackingEnabled) {
        this.trackingEnabled = trackingEnabled;
    }

    public Session getSession() {
        return session;
    }

    public boolean isTrackingEnabled() {
        return trackingEnabled;
    }

    public WeqConfig getWeQConfig() {
        return weQConfig;
    }

    public SdkConfiguration getSdkConfiguration() {
        return sdkConfiguration;
    }
}
