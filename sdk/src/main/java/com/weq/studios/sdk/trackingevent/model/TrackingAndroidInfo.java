package com.weq.studios.sdk.trackingevent.model;

public class TrackingAndroidInfo {

    private String id;
    private String gaid;
    private String board;
    private String bootloader;
    private String brand;
    private String cpuAbi;
    private String deviceId;
    private String display;
    private String manufacturer;
    private String model;
    private String product;
    private String tags;
    private String type;
    private String rotation;

    public TrackingAndroidInfo() {
    }

    public TrackingAndroidInfo(
        String id,
        String gaid,
        String board,
        String bootloader,
        String brand,
        String cpuAbi,
        String deviceId,
        String display,
        String manufacturer,
        String model,
        String product,
        String tags,
        String type,
        String rotation
    ) {
        this.id = id;
        this.gaid = gaid;
        this.board = board;
        this.bootloader = bootloader;
        this.brand = brand;
        this.cpuAbi = cpuAbi;
        this.deviceId = deviceId;
        this.display = display;
        this.manufacturer = manufacturer;
        this.model = model;
        this.product = product;
        this.tags = tags;
        this.type = type;
        this.rotation = rotation;
    }

    public String getGaid() {
        return gaid;
    }

    public String getBoard() {
        return board;
    }

    public String getBootloader() {
        return bootloader;
    }

    public String getBrand() {
        return brand;
    }

    public String getCpuAbi() {
        return cpuAbi;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDisplay() {
        return display;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public String getProduct() {
        return product;
    }

    public String getTags() {
        return tags;
    }

    public String getType() {
        return type;
    }

    public String getRotation() {
        return rotation;
    }
}
