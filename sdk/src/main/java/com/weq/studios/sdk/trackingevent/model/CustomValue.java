package com.weq.studios.sdk.trackingevent.model;

public class CustomValue {
    private String value;
    private String uom;

    public CustomValue(String value, String uom) {
        this.value = value;
        this.uom = uom;
    }
}
