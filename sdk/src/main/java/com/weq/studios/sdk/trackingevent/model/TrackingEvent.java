package com.weq.studios.sdk.trackingevent.model;

public class TrackingEvent {

    private String id;
    private TrackingSession session;
    private WeqEvent event;
    private TrackingTime time;
    private String gameId;
    private TrackingAppInfo appInfo;
    private TrackingDeviceInfo deviceInfo;
    private TrackingConnection connection;

    public TrackingEvent() {
    }

    public TrackingEvent(
        String id,
        TrackingSession session,
        WeqEvent event,
        TrackingTime time,
        String gameId,
        TrackingAppInfo appInfo,
        TrackingDeviceInfo deviceInfo,
        TrackingConnection connection
    ) {
        this.id = id;
        this.session = session;
        this.event = event;
        this.time = time;
        this.gameId = gameId;
        this.appInfo = appInfo;
        this.deviceInfo = deviceInfo;
        this.connection = connection;
    }

    public WeqEvent getEvent() {
        return event;
    }

    public String getId() {
        return id;
    }

    public TrackingSession getSession() {
        return session;
    }

    public TrackingTime getTime() {
        return time;
    }

    public String getGameId() {
        return gameId;
    }

    public TrackingAppInfo getAppInfo() {
        return appInfo;
    }

    public TrackingDeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public TrackingConnection getConnection() {
        return connection;
    }
}
