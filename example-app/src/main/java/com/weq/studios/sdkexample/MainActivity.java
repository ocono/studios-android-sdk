package com.weq.studios.sdkexample;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.weq.studios.sdk.WeqAnalytics;
import com.weq.studios.sdk.trackingevent.model.CustomTag;
import com.weq.studios.sdk.trackingevent.model.EventDefinition;
import com.weq.studios.sdk.trackingevent.model.EventValue;
import com.weq.studios.sdk.common.model.WeqConfig;
import com.weq.studios.sdk.trackingevent.model.WeqEvent;

import java.util.ArrayList;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        WeqConfig config = new WeqConfig(
            this,
            "app-token",
            "gameId"
        );

        WeqAnalytics.configure(config);
        WeqAnalytics.debug(true);

        findViewById(R.id.event).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                WeqAnalytics.event(
                    new WeqEvent(
                        new EventDefinition("cat", "name", new ArrayList<CustomTag>()),
                        new EventValue()
                    )
                );
            }
        });

        findViewById(R.id.stop).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                WeqAnalytics.disableTracking();
            }
        });
        findViewById(R.id.resume).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                WeqAnalytics.enableTracking();
            }
        });

        findViewById(R.id.register_parameter).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                WeqAnalytics.registerParameter("StringTest", "DefaultString", "999", "Description");
            }
        });

        findViewById(R.id.get_parameter).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Log.d("WEQ_EXAMPLE", WeqAnalytics.getParameter("StringTest"));
            }
        });
    }
}


